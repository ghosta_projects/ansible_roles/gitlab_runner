Gitlab runner
=========

This role wil install gitlab-runner on your server. Now register only **shell** and/or **docker** runners.

Requirements
------------

Depending on the runnner choice, nothing special for shell-runner.

Role Variables
--------------

Runner registration token needs to be added, if not present, gitlab-runner will not be registered.

platforms
------------

Ubuntu:     
  versions:     
    - focal (20.04)     
    - jammy (22.04)
